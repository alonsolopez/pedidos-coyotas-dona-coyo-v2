<div>
    <div class="p-5">
    <div class="flex justify-center h-64">
        <div class="relative ">
            <div class="flex flex-row p-2 px-4 truncate rounded cursor-pointer">
                <div></div>
                <div class="flex flex-row-reverse w-full ml-2">
                    <div slot="icon" class="relative">
                        <div class="absolute top-0 right-0 px-1 -mt-1 -mr-2 text-xs font-bold text-white bg-red-700 rounded-full">3</div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="w-6 h-6 mt-2 feather feather-shopping-cart">
                            <circle cx="9" cy="21" r="1"></circle>
                            <circle cx="20" cy="21" r="1"></circle>
                            <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="absolute z-10 w-full border-t-0 rounded-b">
                <div class="w-64 shadow-xl">
                    <div class="flex p-2 bg-white border-b border-gray-100 cursor-pointer hover:bg-gray-100" style="">
                        <div class="w-12 p-2"><img src="https://dummyimage.com/50x50/bababa/0011ff&amp;text=50x50" alt="img product"></div>
                        <div class="flex-auto w-32 text-sm">
                            <div class="font-bold">Product 1</div>
                            <div class="truncate">Product 1 description</div>
                            <div class="text-gray-400">Qt: 2</div>
                        </div>
                        <div class="flex flex-col items-end font-medium w-18">
                            <div class="w-4 h-4 mb-6 text-red-700 rounded-full cursor-pointer hover:bg-red-200">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                </svg>
                            </div>
                            $12.22</div>
                    </div>
                    <div class="flex p-2 bg-white border-b border-gray-100 cursor-pointer hover:bg-gray-100" style="">
                        <div class="w-12 p-2"><img src="https://dummyimage.com/50x50/bababa/0011ff&amp;text=50x50" alt="img product"></div>
                        <div class="flex-auto w-32 text-sm">
                            <div class="font-bold">Product 2</div>
                            <div class="truncate">Product 2 long description</div>
                            <div class="text-gray-400">Qt: 2</div>
                        </div>
                        <div class="flex flex-col items-end font-medium w-18">
                            <div class="w-4 h-4 mb-6 text-red-700 rounded-full cursor-pointer hover:bg-red-200">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                </svg>
                            </div>
                            $12.22</div>
                    </div>
                    <div class="flex p-2 bg-white border-b border-gray-100 cursor-pointer hover:bg-gray-100" style="">
                        <div class="w-12 p-2"><img src="https://dummyimage.com/50x50/bababa/0011ff&amp;text=50x50" alt="img product"></div>
                        <div class="flex-auto w-32 text-sm">
                            <div class="font-bold">Product 3</div>
                            <div class="truncate">Product 3 description</div>
                            <div class="text-gray-400">Qt: 2</div>
                        </div>
                        <div class="flex flex-col items-end font-medium w-18">
                            <div class="w-4 h-4 mb-6 text-red-700 rounded-full cursor-pointer hover:bg-red-200">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                </svg>
                            </div>
                            $12.22</div>
                    </div>
                    <div class="flex justify-center p-4">
                        <button class="flex justify-center px-4 py-2 text-base font-bold text-teal-700 transition duration-200 ease-in-out bg-teal-100 border border-teal-600 rounded cursor-pointer undefined hover:scale-110 focus:outline-none hover:bg-teal-700 hover:text-teal-100">Checkout $36.66</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="h-32"></div>
</div>
</div>
